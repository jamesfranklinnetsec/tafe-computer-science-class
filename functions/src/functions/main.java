package functions;

public class main 
{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(factorialRecursive(1234));
		System.out.println(count7(174757870));
		System.out.println(baseToPower(2,6));
		
		public int static factorialRecursive(int n)
		{
			if (n<=0)
			{
				return 1;
			}
			return n * factorialRecursive(n-1);
		}
		
		/*
		 * Task 2: Creative a recursive function named count7 that
		 * with a given a non-negative int n, return the count of 
		 * the occurrences of 7 as a digit, so for example 717 
		 * yields 2. (no loops). Note that mod (%) by 10 yields the 
		 * rightmost digit (126 % 10 is 6), while divide (/) by 10 
		 * removes the rightmost digit (126 / 10 is 12).
		 */
		
		public int count7 (int n)
		{
			if (n<=0)
			{
				return 0;
			}
			if (n%10 == 7)
			{
				return 1 + count7(n/10);
			}
			return 0 + count7(n/10);
		}

		/*
		 * Task 3: Given base and n that are both 1 or more, compute 
		 * recursively (no loops) the value of base to the n power, 
		 * so powerN(3, 2) is 9 (3 squared).
		 */
		
		public int baseToPower (int x, int y)
		{
			if (y<=0)
			{
				return 1;
			}
			y--;
			return x*baseToPower(x,y);
		}

	}

		
	public class functionClient
	{
		/*
		 * Task 1: Create a recursive function that calculate the 
		 * factorial of number n of type int, the header of the function
		 *  should be
		 *  public int factorial(int n) 
		 *  {
		 *  
		 *  }
		 */
		public int factorialRecursive(int n)
		{
			if (n<=0)
			{
				return 1;
			}
			return n * factorialRecursive(n-1);
		}
		
		/*
		 * Task 2: Creative a recursive function named count7 that
		 * with a given a non-negative int n, return the count of 
		 * the occurrences of 7 as a digit, so for example 717 
		 * yields 2. (no loops). Note that mod (%) by 10 yields the 
		 * rightmost digit (126 % 10 is 6), while divide (/) by 10 
		 * removes the rightmost digit (126 / 10 is 12).
		 */
		
		public int count7 (int n)
		{
			if (n<=0)
			{
				return 0;
			}
			if (n%10 == 7)
			{
				return 1 + count7(n/10);
			}
			return 0 + count7(n/10);
		}

		/*
		 * Task 3: Given base and n that are both 1 or more, compute 
		 * recursively (no loops) the value of base to the n power, 
		 * so powerN(3, 2) is 9 (3 squared).
		 */
		
		public int baseToPower (int x, int y)
		{
			if (y<=0)
			{
				return 1;
			}
			y--;
			return x*baseToPower(x,y);
		}


	}
	public int static factorialRecursive(int n)
	{
		if (n<=0)
		{
			return 1;
		}
		return n * factorialRecursive(n-1);
	}
	
	/*
	 * Task 2: Creative a recursive function named count7 that
	 * with a given a non-negative int n, return the count of 
	 * the occurrences of 7 as a digit, so for example 717 
	 * yields 2. (no loops). Note that mod (%) by 10 yields the 
	 * rightmost digit (126 % 10 is 6), while divide (/) by 10 
	 * removes the rightmost digit (126 / 10 is 12).
	 */
	
	public int count7 (int n)
	{
		if (n<=0)
		{
			return 0;
		}
		if (n%10 == 7)
		{
			return 1 + count7(n/10);
		}
		return 0 + count7(n/10);
	}

	/*
	 * Task 3: Given base and n that are both 1 or more, compute 
	 * recursively (no loops) the value of base to the n power, 
	 * so powerN(3, 2) is 9 (3 squared).
	 */
	
	public int baseToPower (int x, int y)
	{
		if (y<=0)
		{
			return 1;
		}
		y--;
		return x*baseToPower(x,y);
	}

}

	
public class functionClient
{
	/*
	 * Task 1: Create a recursive function that calculate the 
	 * factorial of number n of type int, the header of the function
	 *  should be
	 *  public int factorial(int n) 
	 *  {
	 *  
	 *  }
	 */
	public int factorialRecursive(int n)
	{
		if (n<=0)
		{
			return 1;
		}
		return n * factorialRecursive(n-1);
	}
	
	/*
	 * Task 2: Creative a recursive function named count7 that
	 * with a given a non-negative int n, return the count of 
	 * the occurrences of 7 as a digit, so for example 717 
	 * yields 2. (no loops). Note that mod (%) by 10 yields the 
	 * rightmost digit (126 % 10 is 6), while divide (/) by 10 
	 * removes the rightmost digit (126 / 10 is 12).
	 */
	
	public int count7 (int n)
	{
		if (n<=0)
		{
			return 0;
		}
		if (n%10 == 7)
		{
			return 1 + count7(n/10);
		}
		return 0 + count7(n/10);
	}

	/*
	 * Task 3: Given base and n that are both 1 or more, compute 
	 * recursively (no loops) the value of base to the n power, 
	 * so powerN(3, 2) is 9 (3 squared).
	 */
	
	public int baseToPower (int x, int y)
	{
		if (y<=0)
		{
			return 1;
		}
		y--;
		return x*baseToPower(x,y);
	}


}
