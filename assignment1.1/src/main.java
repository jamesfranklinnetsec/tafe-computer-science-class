//this java program was written by James Franklin, StudentID 804322752, this code runs the following in this order:
//run menus which contain links to use the following items:
//--prime checker
//---gets user input and checks if user input is prime or not using a method isPrime().
//--a method that gets user input and finds all primes between 0 and the user specified input and then outputs them in columns to the terminal
//--an exit method
//--a method that restarts the program


import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.IntStream;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		System.out.print("Welcome to James Franklin's assignment 1 PrimeNumberManager 2020 Enterprise & Education Edition v1.1 \n\n");
		System.out.print("user license key: 696969 \n\n\n\n");
		pressEnterKeyToContinue();
		menuSelection();
		
		//uncomment any of the below 3 methods to test them without going through menuSelection method.
		
		//pressEnterKeyToContinue();
		//primeCheck();
		//primesArrayUpToInput();
		
		
		
		
		
		

	}

	/**
	 * this is the main of the program
	 */
	
	//pressEnterKeyToContinue() function is copied from 
	// https://stackoverflow.com/questions/19870467/how-do-i-get-press-any-key-to-continue-to-work-in-my-java-code
	public static void pressEnterKeyToContinue()
	{ 
	        System.out.println("Press Enter key to continue...");
	        Scanner s = new Scanner(System.in);
	        s.nextLine();
	}
	/**
	 * waits on Enter key to continue.
	 * //pressEnterKeyToContinue() function is copied from 
	 * //https://stackoverflow.com/questions/19870467/how-do-i-get-press-any-key-to-continue-to-work-in-my-java-code
	 */
	
	//exits and tells user the program is closed
	public static void exitProgram()
	{
		System.out.println("\n");
		System.out.println("exiting program");
		
		/*
		 * this try statement simulates having a bloated program need time
		 * to close, so i added a 1 second wait before it exits lol
		 */
		
		/*
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		timer1s();
		
		System.out.println("\n");
		System.out.println("\n");

		System.out.println("program successfully exited (0).");
		System.exit(0);
	}
	/**
	 * exits program
	 */
	
	/*
	 * this code was written to test out lambda functions, I used this
	 * video for reference 
	 * https://www.youtube.com/watch?v=Ee5t_EGjv0A
	 */
	public static void primeCheck()
	{
		System.out.print("Enter an integer: ");
		Scanner sc = new Scanner(System.in); //listening for input in variabl sc
		int number = sc.nextInt(); //turns input of sc into integer
		System.out.println("You enter " + number + " and check if number is prime:");
		System.out.println(isPrime(number));

	}
	
	/**
	 * 	//this code was written to test out lambda functions, I used this video for reference 
	 * https://www.youtube.com/watch?v=Ee5t_EGjv0A
	 */
	
	//lambda function to quickly test if number is prime. can test up to 9 decimal digits.
	private static boolean isPrime(int number) 
	{
		return number > 1 && IntStream.range(2, number).noneMatch(index-> number % index == 0); 
		// above function returns true/false by using a calculated range and using the number against the index of the range, such that every number below the value and above 2 is tried, if the number never modulates to 0, then it is indivisible by anything other than itself and 1, thus prime.
	}
	
	/**
	 * lambda function to quickly test if number is prime. can test up to 9 decimal digits.
	 */

	//takes number, and finds primes up to that number and stores in an array
	public static void primesArrayUpToInput()
	{
		ArrayList listOfPrimes = new ArrayList();
		System.out.print("Enter an integer: ");
		Scanner sc = new Scanner(System.in); //listening for input in variabl sc
		int number = sc.nextInt(); //turns input of sc into integer
		for (int i = 1; i < (number); i++)
		{
			//goes through every number between 0 and input and if its prime, it adds to listOfPrimes
			if (isPrime(i)==true)
			{
				listOfPrimes.add(i);
			}
			i=i+1;
		}
		
		//print the listOfPrimes array to show all primes up to number.	
		System.out.println("list of primes between 0 and "+ number);
		for (int i = 0; i < listOfPrimes.size(); i++)
		{
			//the "\t" will add a tab after printing the numbers, which adjusts the output into a neat column.
			System.out.print(" " + listOfPrimes.get(i) + "\t");
			//every 10th entry gets a new line. this is for readability.
			if (i%10 == 0) 
			{
			System.out.println();
			};

				
					
		}
		System.out.println("\n\nreturning to main menu");
		pressEnterKeyToContinue();
		
	}
	/**
	 * 	//takes number, and finds primes from 0 up to that number and stores in an array and then prints that array.
	 */
	
	//gives the user a range of options to perform
	public static void menuSelection()
	{
		System.out.println("\n\nplease select from this range of options");
		System.out.println(
				"\n"+
				"\t(0) help // displays information about options"+
				"\n"+
				"\t(1) primeCheck()"+
				"\n"+
				"\t(2) primeArrayUpToInput()"+
				"\n"+
				"\t(3) exitProgram()"+
				"\n"+
				"\t(4) main() // restarts program"
				);
		
		System.out.print("\nEnter 0,1,2,3 or 4 and press enter/return to make selection: ");
		Scanner sc = new Scanner(System.in); //listening for input in variabl sc
		int number = sc.nextInt(); //turns input of sc into integer
		if (number >= 0 && number <5)
		{
			if (number == 0)
			{
				System.out.println(
						"\n"+
						"(0) help // displays information about options"+
						"\n"+
						"displays information about options"+
						"\n"+
						"\n"+
						"(1) primeCheck()"+
						"\n"+
						"runs primeCheck() method."+
						"\n"+
						"primeCheck() asks the user for input and then checks if the input is prime."+
						"\n"+
						"\n"+
						"(2) primeArrayUpToInput()"+
						"\n"+
						"runs primeArrayUpToInput() method. "+
						"\n"+
						"primeArrayUpToInput() takes an input number, and finds primes up to that number and stores and prints in an array" + 
						"\n"+
						"\n"+
						"(3) exitProgram()"+
						"\n"+
						"will shutdown the program process."+
						"\n"+
						"\n"+
						"(4) main()"+
						"\n"+
						"restarts program by calling main()"+
						"\n"+
						"\n"
						);
				pressEnterKeyToContinue();


			}
			if (number == 1)
			{
				primeCheck();
			}
			if (number == 2)
			{
				primesArrayUpToInput();
			}
			if (number == 3)
			{
				timer1s();
				
				//using a 1 second timer to make it feel like its loading something big.
				exitProgram();
				//calls exitProgram() method			
			}
			if (number == 4)
			{
				//clearScreen(); //my clearScreen function is buggy, i got it from 
				// https://stackoverflow.com/questions/2979383/java-clear-the-console
				//since it's buggy im just going to print a zillion new lines so flush the screen.
				//i have to flush the screen because if i dont it can look very messy.
				
				
				//adding a timer and clearing screen then another timer, so that you can see its doing stuff over time.
				//it's more satisfying that it takes time to finish the process of reloading the program.
				timer1s();
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
				timer1s();
				main(null);
			}
		}
		else
		{
			System.out.println(number +" is a selection out of range \n\n");
			menuSelection();
		}
		menuSelection();

		
	}
	/**
	 * 	//gives the user a range of options to perform
	 *  //is fabulously complicated
	 */


	public static void clearScreen() {  
	    System.out.print("\033[H\033[2J");  
	    System.out.flush();  
	}  
	/**
	 * clears console ////// doesnt work as expected, nor do i understand what "\033[H\033[2J" is, its obviously some escape characters but idk which
	 */
	public static void timer1s()
	{
		/*
		 * this try statement simulates having a bloated program need time
		 * to do something, for parts of the program where I wanted to make it
		 * feel like it has some weight to it, i use this to add a delay of 1 second
		 */
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * 1 second timer
	 */
	
	
}

//next time I might refactor up my menu section more, 
//as it seems to need to be broken up into more methods
//so that it's easier to skim through.
