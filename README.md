# TAFE computer science class 

Collection of java programs I wrote made for my term 1 2020 computer science class. They're all quite amateur.

## About

Please visit the file ITICT207A_SubjectGuide.pdf for detail on learning outcomes and so on.

## Contributing
Please don't contribute. This is a proof of work.

## License
[MIT](https://choosealicense.com/licenses/mit/)
