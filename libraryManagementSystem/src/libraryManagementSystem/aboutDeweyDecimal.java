package libraryManagementSystem;



/*
 * The Dewey Decimal System is a system wherein there are
 * 10 classes of literary categories. Within these 10 literary
 * categories, there are 10 further divisions, each divided
 * further into 10 more section for specificity.
 * 
 * here are the top 10 classes:
 * 000 - computer science, information & general works
 * 100 - philosophy & psychology
 * 200 - religion
 * 300 - social sciences
 * 400 - language
 * 500 - science
 * 600 - technology
 * 700 - arts & recreation
 * 800 - literature
 * 900 - history & geography
 * 
 * lets take 000 and see whats inside it as an example.
 * 000 - computer science, knowledge & systems
 * 010 - bibliographies
 * 020 - library & information sciences
 * 030 - encyclopedias & books of facts
 * 040 - unassigned
 * 050 - magazines, journals & serials
 * 060 - associations, organisations & museums
 * 070 - news media, jounralism & publishing
 * 080 - quotations
 * 090 - manuscripts & rare books
 * 
 * further subdivided into the next row 000 we can see these subjects
 * 000 - computer science, information & general works
 * 001 - knowledge
 * 002 - the book
 * 003 - systems
 * 004 - data processing & computer science
 * 005 - computer programming, programs & data
 * 006 - special computer methods
 * 007 - unassigned
 * 008 - unassigned
 * 009 - unassigned
 * 
 * so, the purpose of this program will be to categorise
 * a book into giving it a dewey decimal number.
 * 
 * I also plan to make this a nice program.	
 */
public class aboutDeweyDecimal {
	

}
