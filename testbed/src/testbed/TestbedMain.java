package testbed;

import java.util.Scanner;

public class TestbedMain {

	public static void main(String[] args) {
		
		
		//inputting and verifying the operating number
		
		//retrieving the integer to work with
		Scanner in = new Scanner(System.in);
		System.out.print("input: ");
		//setting variable 'val' as the store for the integer
		int val = in.nextInt();
		in.close();
		//displaying val to verify to user that the
		System.out.printf(("your value is: ")+ val);
		
		
		//counting length of the value
		
		/*turn val into a string, then perform length on
		 *  val, as strings are arrays and
		thus their length can be counted as such*/
		int numLength = String.valueOf(val).length();
		System.out.printf(("the length of your value is: ")+ numLength);
		
		
		//use length of integer to divide the integer into its constituent numbers
		
		/*
		 * seeing online a method that looks like this, I will try to make a more
		 * extensible version
		*/
		
		/*	

		int n1 = input / 100000 % 10;
        int n2 = input / 10000 % 10;
        int n3 = input / 1000 % 10;
        int n4 = input / 100 % 10;
        int n5 = input / 10 % 10;
        int n6 = input % 10;
        
        */
		
		/*the issue with this, is that n1->n6 are manually allocated. I want to have
		 *the variables generated as needed, for example if i have a var with 4 digits,
		 *such as '4096', i would only need n1->n4. so I think I should make a method
		 *for this operation.
		 */
		
		//method to generate operations performed on input
		int x=0;
		int A = (Integer) null;
		while (x<numLength) {
			A = ((val /(10^(x)) % 10));
			x++;
		
			}
		System.out.print(A);
		}
		
	}


