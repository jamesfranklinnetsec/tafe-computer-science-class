package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenu extends JFrame {
	public MainMenu (String title) {
		super (title);
		
		//set layout manager
		setLayout (new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS)); //box layout on y axis aligns items top to bottom.
		
		
		//create swing components
		JTextArea blurb = new JTextArea("please select an item.");
		
		JButton b1 = new JButton("go to welcome screen");
		JButton b2 = new JButton("add entry");
		JButton b3 = new JButton("view catalogue");
		JButton b4 = new JButton("kill process");
		
		
		
		
		
		//add swing components to content pane
		Container c = getContentPane();
		c.add(blurb, BoxLayout.X_AXIS);
		c.add(b4,BoxLayout.X_AXIS);
		c.add(b3,BoxLayout.X_AXIS);
		c.add(b2,BoxLayout.X_AXIS);
		c.add(b1,BoxLayout.X_AXIS);
		
		
		
		//set behaviours
		
		//behaviour for button labelled 'go to welcome screen'
		b1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				Main.welcome();
				dispose();
			}
		});
		
		//behaviour for button labelled 'add entry'
		b2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				Main.addEntry();
				dispose();
			}
		});
		//behaviour for button labelled 'view catalogue'
		b3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				Main.catalogue();
				dispose();
			}
		});

		
		//behaviour for button labelled 'kill process'
		b4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});



		
		
	}

}
