package GUI;
import javax.swing.*;
import java.awt.*;

public class EntryPanel extends DetailsPanel {
	public EntryPanel() {
		
		//setting custom size of panel
		//use for debugging Field properties
		/*
		Dimension size = getPreferredSize();
		size.width = 450;
		setPreferredSize(size);
		*/
		



		
		//adding components
		
		
		
		setBorder(BorderFactory.createTitledBorder("entry panel"));
		JLabel nameLabel = new JLabel("Name:");
		
		JLabel titleLabel = new JLabel("Title:");
		JLabel descriptionLabel = new JLabel("Description:");
		JLabel deweyvalueLabel = new JLabel("Dewey Decimal:");
				
		
		int lengthOfField = 10;
		
		JTextField nameField = new JTextField(lengthOfField);
		JTextField titleField = new JTextField(lengthOfField);
		JTextField descriptionField = new JTextField(lengthOfField);
		JTextField deweyvalueField = new JTextField(lengthOfField);
				
		JButton submitBtn = new JButton("submit entry");
		JButton returnBtn = new JButton("return to menu");
		JButton exitBtn = new JButton("exit program");

		
		//setting layout before using the content pane
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		//adding components to content pane

		//// column 1 - Labels////
		
		
		gc.anchor = GridBagConstraints.LINE_START; //align to line end, which is next to the fields
		
		gc.weightx = 0; //weight gives space surrounding items
		gc.weighty = 0; 
		
		
		gc.gridx = 0; //location in Grid
		gc.gridy = 0;
		add(nameLabel, gc);
		
		gc.gridx = 0;
		gc.gridy = 1;
		add(titleLabel, gc);
		
		gc.gridx = 0;
		gc.gridy = 2;
		add(descriptionLabel, gc);
		
		gc.gridx = 0;
		gc.gridy = 3;
		add(deweyvalueLabel, gc);


		//// column 2 - Fields ////
		
		
		gc.anchor = GridBagConstraints.LINE_END; //align to line start, which is next to the labels
		
		gc.gridx = lengthOfField;
		gc.gridy = 0;
		add(nameField, gc);
		
		gc.gridx = lengthOfField;
		gc.gridy = 1;
		add(titleField, gc);
		
		gc.gridx = lengthOfField;
		gc.gridy = 2;
		add(descriptionField, gc);
		
		gc.gridx = lengthOfField;
		gc.gridy = 3;
		add(deweyvalueField, gc);
		
		//// commit row ////
		gc.anchor = GridBagConstraints.LINE_END; //i want to align the button to the bottom of the panel.
		gc.gridy = 4; //large number so it is always at bottom
		gc.weighty = 0; //to change weight after fields and labels section
		
		gc.gridx = 10;
		add(submitBtn, gc);

		gc.anchor = GridBagConstraints.LINE_END;
		gc.gridy = 5;
		add(returnBtn, gc);
		
		gc.anchor = GridBagConstraints.PAGE_END;
		gc.weighty = 100;
		gc.gridy = 6;
		add(exitBtn, gc);
		
		
		

		
		
		
		//setting behaviours
		

		
	}
	

}
