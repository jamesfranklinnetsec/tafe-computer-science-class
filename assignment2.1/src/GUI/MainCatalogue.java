package GUI;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class MainCatalogue extends JFrame {
	public MainCatalogue(String title) {
		super (title);
		//set layout manager
		
		
		setLayout(new BorderLayout());
		
		//create swing components
		
		
		JTextArea textArea = new JTextArea();
		JButton button = new JButton("continue...");
		
		//add swing components to content pane
		
		
		Container c = getContentPane();
		
		c.add(textArea, BorderLayout.CENTER);
		c.add(button, BorderLayout.SOUTH);

	}

}
