package GUI;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//addEntry();
		welcome();
	}
	
	
	/**
	 * runs a welcome GUI	
	 */
	public static void welcome() {
		JFrame welcome = new MainWelcome("welcome");
		welcome.setSize(500,400);
		welcome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		welcome.setVisible(true);

	}
	/**
	 * runs a menu GUI
	 */
	public static void menu() {
		JFrame menu = new MainMenu("menu");
		menu.setSize(800,800);
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.setVisible(true);

	}
	/**
	 * runs an entry GUI
	 */
	public static void addEntry() {
		JFrame addEntry = new MainAddEntry("add entry");
		addEntry.setSize(500,400);
		addEntry.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addEntry.setVisible(true);

	}
	/**
	 * runs a catalogue GUI
	 */
	public static void catalogue() {
		JFrame catalogue = new MainCatalogue("catalogue");
		catalogue.setSize(1000,800);
		catalogue.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		catalogue.setVisible(true);

	}
	
	

		


}
