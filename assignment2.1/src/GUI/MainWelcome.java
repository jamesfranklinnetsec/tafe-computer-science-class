package GUI;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.*;
import java.awt.*;


public class MainWelcome extends JFrame {
	public MainWelcome(String title) {
		super(title);
		
		//set layout manager
		
		
		setLayout(new BorderLayout());
		
		//create swing components
		
		
		JTextArea textArea = new JTextArea();
		JButton button = new JButton("continue...");
		
		//add swing components to content pane
		
		
		Container c = getContentPane();
		
		c.add(textArea, BorderLayout.CENTER);
		c.add(button, BorderLayout.SOUTH);
		
		
		//add behaviour -- open a new frame.
		
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				Main.menu();
				dispose();
			}
		});
		
	}
}
