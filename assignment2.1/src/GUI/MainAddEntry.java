package GUI;

import java.awt.*;

import javax.swing.*;

public class MainAddEntry extends JFrame {
	
	private EntryPanel entryPanel;
	
	public MainAddEntry (String title) {
		super (title);
		
		//set layout manager
		setLayout(new BorderLayout());
		
		//create swing components
		entryPanel = new EntryPanel();
		
		//add swing components to content pane
		Container c = getContentPane();
		c.add(entryPanel, BorderLayout.WEST);
		
		//set behaviours
	}
	

}
