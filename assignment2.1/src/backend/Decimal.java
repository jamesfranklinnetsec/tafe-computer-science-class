package backend;
public class Decimal {
	int storedValue;
	/*
	public Decimal(int value) {
		storedValue = value;
	}

	public static Decimal of (int value){
		if (value < 0 || value > 9) throw new IllegalArgumentException();
		return new Decimal(value);

	}
	private void Set (int input) {
		value = input;
	}
	*/
	
	private int D;

	public int getD() {
		return D;
	}

	public void setD(int d) {
		if (d < 0 || d > 9) throw new IllegalArgumentException(); //allow only integers between 0 and 9
		D = d;
	}
	
}

