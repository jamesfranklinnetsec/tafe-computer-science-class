package javaSwingTest;

import java.awt.Dimension;
import java.awt.GridBagConstraints;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class WelcomeSplashPanel extends JPanel 
{
	public WelcomeSplashPanel()
	{
		
		//this sets the size of the panel.
		Dimension size = getPreferredSize();
		size.width = 250;
		setPreferredSize(size);
		
		setBorder(BorderFactory.createTitledBorder("Welcome Splash Screen"));
		
		JLabel welcomeLabel = new JLabel ("welcome to my program");
		JLabel welcomeBlurbLabel = new JLabel ("this is my program\nblablabla\nblablabla");
		
		JButton welcomeContinueButton = new JButton ("continue...");
		
		GridBagConstraints gc = new GridBagConstraints();
		
		//// Welcome Label ////
		
		gc.weightx = 0.1;
		gc.weighty = 0.9;
		
		gc.gridx = 0;
		gc.gridy = 0;
		add(welcomeLabel, gc);
		
		//// WelcomeBlurbLabel ////
		
		gc.weightx = 0.9;
		gc.weighty = 0.9;
		
		gc.gridx = 0;
		gc.gridy = 4;
		add(welcomeBlurbLabel, gc);
		
		//// welcomeContinueButton
		
		gc.weightx = 0.5;
		gc.weighty = 0.5;
		gc.gridx = 0;
		gc.gridy = 0;
		
		add(welcomeContinueButton, gc);
		

		

	}

}
