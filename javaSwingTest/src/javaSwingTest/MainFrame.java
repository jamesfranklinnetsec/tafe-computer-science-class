package javaSwingTest;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class MainFrame extends JFrame 
{
	private DetailsPanel detailsPanel;
	
	private WelcomeSplashPanel welcomeSplashPanel;
	
	public MainFrame(String title) 
	{
		super(title);
		
		// set layout manager
		setLayout(new BorderLayout());
		// create swing components
		final JTextArea textArea = new JTextArea();
		JButton button = new JButton("click me!");
		
		detailsPanel = new DetailsPanel();
		welcomeSplashPanel = new WelcomeSplashPanel();
		
		//add swing components to content pane
		Container c = getContentPane();
		
		c.add(textArea, BorderLayout.CENTER);
		c.add(button, BorderLayout.SOUTH);
		c.add(detailsPanel, BorderLayout.WEST);
		c.add(welcomeSplashPanel, BorderLayout.EAST);
		
		// add behaviour
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				textArea.append("Hello\n");
			}
			
			
			
				});
		
		
	}

} 
