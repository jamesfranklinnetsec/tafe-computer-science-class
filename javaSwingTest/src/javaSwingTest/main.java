package javaSwingTest;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;


public class main {

	public static void main(String[] args) {
		
		SwingUtilities.invokeLater //invokeLater will run my swing code in a special swing thread
		(new Runnable() 
		{
			public void run() //just making myself a method to put my swing code inside of, which is nice.
			{
				JFrame frame = new MainFrame("Hello World Swing!");
				frame.setSize(500,400);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);

				
			}
		});
		/*
		JFrame frame = new JFrame("Hello World Swing!");
		frame.setSize(500,400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		*/
		
	}

}
 