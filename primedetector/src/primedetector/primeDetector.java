package primedetector;

import java.util.Scanner;
import java.util.stream.IntStream; 

public class primeDetector {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String isitprime = Scanner.nextLine();
		System.out.println(primeTest(isitprime));
		
	}
	
	private static boolean primeTest(final int number) {
		return number > 1 &&
				IntStream.range(2, number)
						 .noneMatch(index -> number % index == 0);
	}

}
