package arraytests;

import java.util.ArrayList;

public class arrayMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList();
		list.add(5);
		list.add(10);
		list.add(15);
		list.add(20);
		list.add(-7);
		list.add(-14);
		list.add(-22);
		list.add(-11);
		
		System.out.println(list.get(3));
		list.set(3, -11);
		System.out.println(list.get(3));
		list.remove(3);
		System.out.println(list.get(3));


		int indexNeg14 = list.indexOf(-14);
		int indexNeg22 = list.indexOf(-22);
		
		System.out.println(indexNeg14);
		System.out.println(indexNeg22);
		
		list.add(indexNeg14, 24);
		
		
		
		
		System.out.println("the arrays length is of size "+ list.size());
		
		System.out.println("this is the contents of the list ");
		for (int i=0; i<list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		/*
		//this is a lambda function implementation of the above but I can't figure why it's broken
		
		
		list.forEach(
				list -> System.out.println(
						list.get(
								)
						)
				);
				
				
				
		*/

		//sum of all the variables in the array list.
		
		int listLength = list.size();
		
		int listSumTally = 0;
		
		
		for (int i=0; i<listLength; i++) {
			listSumTally = listSumTally + list.get(i);
		}
		System.out.println(listSumTally);
		


	}

}
