package recursiveDemo;

public class functionRevisionClient 
{
	functionRevision myR = new functionRevision();
	
	/**
	 * This function should get a number and return its square
	 * @param n this is the input number
	 * @return the square of the input number
	 */
	public static double sqr(double n)
	{
		return n * n; 
	}
	
	
	/**
	 * This function gets a number and return true if the number is even
	 * it returns false otherwise
	 * @param n the input number
	 * @return true if the number is even, false otherwise.
	 */
	public static boolean isEven(double n)
	{
		if (n % 2 == 0)
		{
			return true;
		}
		return false;
	}
	
	
	/**
	 * this function will get a number and return true if the last digit is even, it returns false otherwise.
	 * @param n this is the integer input number
	 * @return whether n is even or not
	 */
	public boolean isFirstEven(int n)
	{
		int first = (n%10);
		if (first % 2  == 0)
		{
			return true;
		}
		return false;
	}
	
	/*
	 * 
	 */
	public double addAllBlake(double n)
	{
		double total =n;
		while(n>=1)
		{
			--n;
			total = total + n;
		}
		return total;
	}
	
	
	/*
	 * the O notation to execute this function is O(n)
	 */
	public double addAll(double n)
	{
		double result=0;
		for (int i=1; i<=n;i++)
		{
			result+=i;
		}
		return result;
	}
	
	
	/**
	 * 
	 * @param n the input number
	 * @return the sum of sll the number til 1
	 */
	public double addAllRecursive(double n)
	{
		if (n<=0)
		{
			return 0;
		}
		return n + addAllRecursive(n-1);
	}
	
	
	/*
	 * create a recursive function that gets
	 * a number and returnss how many digits
	 * ex: if given 1298 it will return 4
	 */
	
	public long countDigit(long n) 
	{
		if (n==0)
		{
			return 0;
		}
		return 1 + countDigit(n / 10);
	}
	
	
	/*
	 * create a recursive function that gets
	 * a number and returns how many _even_ digits
	 * in this number:
	 * ex: if the input is 12345 it should return 2 (2,4)
	 * ex2: if the input is 1026 it should return 3 (0,2,6)
	 * ex3: if the input is 1426 it should return 3 (4,2,6)
	 */
	
	// if (n==0) {return count) n%2==0 -> count++; n/10
	
	public int countEvenDigits(int n)
	{
		if (n==0)
		{
			return 0;
		}
		if ((n%10)%2==0)
		{
			return 1 + countEvenDigits(n/10);
		}
		else return 0 + countEvenDigits(n/10);
		
		
	}
	
	/*
	 * Task 1: Create a recursive function that calculate the 
	 * factorial of number n of type int, the header of the function
	 *  should be
	 *  public int factorial(int n) 
	 *  {
	 *  
	 *  }
	 */
	public int factorialRecursive(int n)
	{
		if (n<=0)
		{
			return 1;
		}
		return n * factorialRecursive(n-1);
	}
	
	/*
	 * Task 2: Creative a recursive function named count7 that
	 * with a given a non-negative int n, return the count of 
	 * the occurrences of 7 as a digit, so for example 717 
	 * yields 2. (no loops). Note that mod (%) by 10 yields the 
	 * rightmost digit (126 % 10 is 6), while divide (/) by 10 
	 * removes the rightmost digit (126 / 10 is 12).
	 */
	
	public int count7 (int n)
	{
		if (n<=0)
		{
			return 0;
		}
		if (n%10 == 7)
		{
			return 1 + count7(n/10);
		}
		return 0 + count7(n/10);
	}

	/*
	 * Task 3: Given base and n that are both 1 or more, compute 
	 * recursively (no loops) the value of base to the n power, 
	 * so powerN(3, 2) is 9 (3 squared).
	 */
	
	public int baseToPower (int x, int y)
	{
		if (y<=0)
		{
			return 1;
		}
		y--;
		return x*baseToPower(x,y);
	}
	
	
	//counts number of As in string.
	public int countA(String s)
	{
		if(s.length()==0)
		{
			return 0;
		}
		else
		{
			if (s.charAt(0)=='a')
			{
				return 1 + countA(s.substring(1));
			}
			else
			{
				return countA(s.substring(1));
			}
		}
	}
	
	
	/*
	 * Create a recursive function that gets a string and returns how many
	 * "me" string in the original string
	 * ex: if input is "meomeume" it should return 3
	 * 
	 * toUpperCase
	 * toLowerCase
	 */
	
	
	/**
	 * a small modification to the countA function.
	 * 
	 * @param s takes string
	 * @return how many counts of me
	 */
	public int countMe(String s)
	{
		if(s.length()==0)
		{
			return 0;
		}
		else
		{
			if (s.charAt(0)==('m'))
			{
				if (s.charAt(1)==('e'))
				{
					return 1 + countMe(s.substring(1));
				}
				//return 1 + countMe(s.substring(1));
				return countMe(s.substring(1));
			}
			else
			{
				return countMe(s.substring(1));
			}
		}
	}

	

}
