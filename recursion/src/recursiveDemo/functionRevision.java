package recursiveDemo;

public class functionRevision 
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		functionRevisionClient myR = new functionRevisionClient();
		//System.out.println(myR.isFirstEven(8947));
		System.out.println(myR.baseToPower(2,6));
		System.out.println(myR.count7(777077123));
		System.out.println(myR.factorialRecursive(6));
		System.out.println(myR.countMe("dklfmmeeemememe"));

	}
	
	/**
	 * This function should get a number and return its square
	 * @param n this is the input number
	 * @return the square of the input number
	 */
	
	/*
	public static double sqr(double n)
	{
		return n * n; 
	}
	*/
	
	/**
	 * This function gets a number and return true if the number is even
	 * it returns false otherwise
	 * @param n the input number
	 * @return true if the number is even, false otherwise.
	 */
	/*
	public static boolean isEven(double n)
	{
		if (n % 2 == 0)
		{
			return true;
		}
		return false;
	}
	*/
	
	

}
