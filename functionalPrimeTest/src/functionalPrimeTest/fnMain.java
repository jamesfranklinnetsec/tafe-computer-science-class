package functionalPrimeTest;

import java.util.stream.IntStream;
import java.util.*;
import java.util.Scanner;


public class fnMain {
	static Scanner sc = new Scanner(System.in); //setting up input
	public static void main(String[] args) {
		System.out.print("Enter an integer: ");
		int number = sc.nextInt();
		System.out.println("You enter " + number + ".");
		System.out.println(isPrime(number));
		//System.out.println(factorisers(number));

		}
		public static boolean isPrime(int number) {
			return number > 1 && IntStream.range(2, number).noneMatch(index-> number % index == 0); 
			// above function returns true/false by using a calculated range and using the number against the index of the range, such that every number below the value and above 2 is tried, if the number never modulates to 0, then it is indivisible by anything other than itself and 1, thus prime.
			

		}
		/*
		public static int[] factors (int number) {
			if(isPrime(number)) { //no point in showing factors if there are none, so the program does that first before recording the factors
				
			}
			
		}
		*/


	}

