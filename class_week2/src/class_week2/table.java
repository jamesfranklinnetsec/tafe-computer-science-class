package class_week2;

class table
{
	//declaring variables
	public int width;
	public int height;
	
	
	// to give default values to the variables
	public table()
	{
		width=5;
		height=5;
	}
	
	//creating function for tableArea
	int tableArea (int width, int height) 
	{
		return width*height;
	}
}