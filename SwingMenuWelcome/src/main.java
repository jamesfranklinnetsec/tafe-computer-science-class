import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import javaSwingTest.MainFrame;

public class main {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run () {
				JFrame welcomeFrame = new WelcomeFrame("Welcome!");
				WelcomeFrame.setSize(500,400);
				WelcomeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				WelcomeFrame.setVisible(true);

			}
		});
	}

}
