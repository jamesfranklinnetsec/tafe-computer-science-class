import java.awt.BorderLayout;
import java.awt.Container;

public class WelcomeFrame extends JFrame {
	private WelcomePanelSplash welcomePanel;
	
	public WelcomeFrame (String title) {
		super (title);
		
		//set layout manager
		setLayout(new BorderLayout());
		
		//create swing components
		final JTextArea textArea = new JTextArea();
		JButton continueButton = new JButton("continue...");
		
		welcomePanel = new WelcomePanel();
		
		//add swing components to content pane
		Container C = getContentPane();
		
		C.add(textArea, BorderLayout.CENTER);
		C.add(continueButton, BorderLayout.SOUTH);
		C.add(welcomePanel, BorderLayout.SOUTH);

	}
	

}
