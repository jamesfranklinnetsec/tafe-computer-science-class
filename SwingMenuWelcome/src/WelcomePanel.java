package
import java.awt.Dimension;

import javax.swing.JPanel;

public class WelcomePanel extends JPanel {
	public WelcomePanel() {
		
		//this sets the size of the panel.
		Dimension size = getPreferredSize();
		size.width = 250;
		setPreferredSize(size);

	}

}
