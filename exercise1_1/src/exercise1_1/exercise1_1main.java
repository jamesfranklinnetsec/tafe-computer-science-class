package exercise1_1;

import java.util.Scanner;

public class exercise1_1main {
	  public static void main(String[]args){
	    //write code here
	    Scanner in = new Scanner(System.in);
	    System.out.print("Input the side length value: ");
	    double val = in.nextDouble();
	    System.out.printf("Square: %12.2f\n", val * val);
	    System.out.printf("Cube: %14.2f\n", val * val * val);
	  }
	}
