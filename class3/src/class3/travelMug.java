package class3;

public class travelMug {
	private float volume;
	
	public float volumeGet() {
		return volume;
		
	}
	public void volumeSet(float v) {
		volume = v;
				
	}

	public travelMug() {
		volume = 100;
		
	}
	
	public float literToGallons(float liter) {
		float result =0;
		result = liter * (float) 4.6;
		return result;
	}
	
}
